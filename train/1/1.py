import pandas as pd
import numpy as np
from fbprophet import Prophet
import matplotlib.pyplot as plt
import csv
df = pd.read_csv('./data/total_redeem.csv')
df.head()
m = Prophet()
m.fit(df)
future = m.make_future_dataframe(periods=30)
future.tail()
forecast = m.predict(future)
print(forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']])
forecast_csv=forecast[['ds','yhat']]
forecast_csv.to_csv('train1_pur_redeem.csv',index=False)
m.plot(forecast)
#m.plot_components(forecast)
x1 = forecast['ds']
y1=forecast['yhat']
plt.plot(x1,y1)
plt.show()

